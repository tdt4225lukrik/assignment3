from DbConnector import DbConnector
from datetime import datetime
import os

connection = DbConnector()
client = connection.client
db = connection.db
collection = db["Activity"]

USER_COUNT = 182
def get_all_users():
    # Add all users
    users = {}
    for i in range(USER_COUNT):
        user_id = str(i).rjust(3, "0")
        users[user_id] = (user_id, False)   # (user_id, has_labels)

    # Update everyone that have labels
    label_file = open("dataset\labeled_ids.txt")
    for line in label_file.readlines():
        # Find user with id of line and set has_labels to True 
        users[line.strip()] = (line.strip(), True)
    label_file.close()

    return users


# A dict of all users and has_labels
users = get_all_users()

def walk():
    dir_it = os.walk("dataset\Data")
    
    # Jump over root case
    next(dir_it)

    # Iterate through all User folders
    while dir_it:
        try:
            dirpath, dirnames, filenames = next(dir_it)
        except StopIteration:
            break
        
        # Get User Id
        user_id = dirpath[-3:]
        print(user_id)

        # Go through labels (labels.txt)
        labels = []
        if "labels.txt" in filenames:
            file = open(dirpath + "\\" + "labels.txt")
            lines = file.readlines()

            # Jump over header
            lines = lines[1:]

            for line in lines:
                row = line.strip().split("\t")
                # Convert times to datetime
                start_date_time = datetime.strptime(row[0], "%Y/%m/%d %H:%M:%S")
                end_date_time = datetime.strptime(row[1], "%Y/%m/%d %H:%M:%S")

                mode = row[2]
                label = (start_date_time, end_date_time, mode)
                labels.append(label)

            file.close()
        
        # Iterate through all files in Trajectory folder
        dirpath, dirnames, filenames = next(dir_it)
        activities = []
        for filename in filenames:
            file = open(dirpath + "\\" + filename)
            lines = file.readlines()
            
            # Do not include file if over 2500 lines
            if len(lines) >= 2500 + 6:
                continue
            
            # Remove 6 first lines
            lines = lines[6:]

            # Set some of the activity variables
            start_row = lines[0].strip().split(",")
            end_row = lines[-1].strip().split(",")
            start_date_time = datetime.strptime(start_row[5] + " " + start_row[6], "%Y-%m-%d %H:%M:%S")
            end_date_time = datetime.strptime(end_row[5] + " " + end_row[6], "%Y-%m-%d %H:%M:%S")
            transportation_mode = None

            # Check if there are labels
            label_match = list(filter(lambda label: label[0] == start_date_time and label[1] == end_date_time, labels))
            if len(label_match) > 0:
                transportation_mode = label_match[0][2] # the transportation mode on the label

            # print(activity.id, activity.user_id, activity.transportation_mode, activity.start_date_time, activity.end_date_time)


            # Go through lines of file and create list of TrackPoint
            track_points = []
            for line in lines:
                # Create TrackPoint
                row = line.strip().split(",")
                lat = float(row[0])
                lon = float(row[1])
                altitude = int(float(row[3]))
                date_time = datetime.strptime(row[5] + " " + row[6], "%Y-%m-%d %H:%M:%S")
                track_point = {
                    "lat": lat,
                    "lon": lon,
                    "altitude": altitude,
                    "date_time": date_time
                }

                track_points.append(track_point)

                # print(line.strip())
            
            # Insert the activity with all the trackpoints as an array
            activity = {
                "user": {
                    "id": user_id,
                    "has_labels": users[user_id][1]
                },
                "transportation_mode": transportation_mode,
                "start_date": start_date_time,
                "end_date": end_date_time,
                "trackPoints": track_points
            }

            collection.insert(activity)

            # print(activity)
            

            file.close()
        


# add_users()
walk()