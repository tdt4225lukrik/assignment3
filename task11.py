"""
Find the top 20 users who have gained the most altitude meters.
1. Output should be a table with (id, total meters gained per user).
2. Remember that some altitude-values are invalid
3. Tip: (tpn.altitude - tpn-1.altitude), tpn.altitude > tpn-1.altitude
"""
from DbConnector import DbConnector
from pprint import pprint
from tabulate import tabulate

collection = DbConnector().db["Activity"]

FEET_TO_METERS = 3.2808399

user_gains = []
for i in range(182):
    user_id = (str(i).rjust(3, "0"))
    # Find all the activities for the specific user (don't want to take all activities as it will take alot of memory)
    user_activities = collection.aggregate([
        {
            '$match': {
                'user.id': user_id
            }
        }
    ])

    total_gain = 0
    # Go through all activities
    for activity in user_activities:
        found_invalid = False # if we have found a violation for the current activity
        
        # Go through all trackpoints in the activity and compare each subsequent trackpoint
        for j in range(1, len(activity["trackPoints"])):
            
            # Do not try to look for violation in the activty after we have found one
            if found_invalid:
                break

            last_tp = activity["trackPoints"][j - 1]
            current_tp = activity["trackPoints"][j]

            # check for invalid altitude values
            if last_tp["altitude"] == -777 or current_tp["altitude"] == -777:
                continue
            
            # Do the altitiude gain calculations
            alt_gain = current_tp["altitude"] - last_tp["altitude"]
            if alt_gain >= 0:
                total_gain += alt_gain
        
    print(user_id, total_gain * FEET_TO_METERS)
    user_gains.append((user_id, total_gain * FEET_TO_METERS))

list_result = list(sorted(user_gains, key=lambda user: user[1], reverse=True))[0:20]
print(tabulate(list_result, headers=["user_id", "Altitude gain in meters"]))