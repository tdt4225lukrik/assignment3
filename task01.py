"""
How many users, activities and trackpoints are there in the dataset (after it is
inserted into the database).
"""

from pprint import pprint
from DbConnector import DbConnector

collection = DbConnector().db['Activity']

result = list(collection.aggregate([
    {
        '$facet': {
            "users" : [{'$group': {'_id': '$user.id'}}, {'$count': 'count'}],
            "activities" : [{'$count': 'count'}],
            "trackpoints" : [{'$unwind': {'path': '$trackPoints'}}, {'$count': 'count'}]
        }
    }
]))

pprint(result)