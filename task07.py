"""
Find all users that have never taken a taxi.
"""

from pprint import pprint
from DbConnector import DbConnector

collection = DbConnector().db["Activity"]

result = list(collection.aggregate([
    {
        '$facet': {
            'all_users': [
                {
                    '$group': {
                        '_id': '$user.id'
                    }
                }
            ], 
            'taken_taxi': [
                {
                    '$match': {
                        'transportation_mode': 'taxi'
                    }
                }, {
                    '$group': {
                        '_id': '$user.id'
                    }
                }
            ]
        }
    }, {
        '$project': {
            'not_taken_taxi': {
                '$setDifference': [
                    '$all_users', '$taken_taxi'
                ]
            }
        }
    }
]))
# Prints without the "not taken taxi" field name, and sorts it for readability
print(sorted(result[0]['not_taken_taxi'], key=lambda x: x['_id']))