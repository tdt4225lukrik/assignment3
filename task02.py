"""
Find the average, minimum and maximum number of activities per user.
"""
from DbConnector import DbConnector
from pprint import pprint

collection = DbConnector().db["Activity"]

result = collection.aggregate([
    { 
        "$group": {
            "_id": "$user.id",
            "totalActivities": {
                "$sum": 1
            }
        }
    },
    { 
        "$group": {
            "_id": "",
            "max": {
                "$max": "$totalActivities"
            },
            "min": {
                "$min": "$totalActivities"
            },
            "avg": {
                "$avg": "$totalActivities"
            }
        }
    }
])

pprint(list(result))