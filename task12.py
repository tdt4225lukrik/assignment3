"""
Find all users who have invalid activities, and the number of invalid activities per user
1. An invalid activity is defined as an activity with consecutive trackpoints
where the timestamps deviate with at least 5 minutes.
"""
from DbConnector import DbConnector
from tabulate import tabulate

collection = DbConnector().db["Activity"]

# Go through all users
invalid_users = set({})
results = []

for i in range(182):
    user_id = (str(i).rjust(3, "0"))
    # Find all the activities for the specific user (don't want to take all activities as it will take alot of memory)
    user_activities = collection.aggregate([
        {
            '$match': {
                'user.id': user_id
            }
        }
    ])

    invalid_actitivites = 0
    # Go through all activities to try to find some invalid activities
    for activity in user_activities:
        found_invalid = False # if we have found a violation for the current activity
        
        # Go through all trackpoints in the activity and compare each subsequent trackpoint in time
        for j in range(1, len(activity["trackPoints"])):
            
            # Do not try to look for violation in the activty after we have found one
            if found_invalid:
                break

            # Do the time comparison
            last_tp = activity["trackPoints"][j - 1]
            current_tp = activity["trackPoints"][j]
            diff = (current_tp["date_time"] - last_tp["date_time"]).total_seconds() / 60
            if diff >= 5:
                # print(last_time, current_time, diff)
                invalid_users.add(user_id)
                invalid_actitivites += 1
                found_invalid = True

    # Add to results
    if user_id in invalid_users:
        print((user_id, invalid_actitivites))
        results.append((user_id, invalid_actitivites))


print(tabulate(results, headers=["user_id", "Invalid activities"]))