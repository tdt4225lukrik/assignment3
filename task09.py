"""
a) Find the year and month with the most activities.
b) Which user had the most activities this year and month, and how many
recorded hours do they have? Do they have more hours recorded than the user
with the second most activities?
"""

from pprint import pprint
from DbConnector import DbConnector

connection = DbConnector().db['Activity']

topYearMonth = connection.aggregate([
    {
        '$group': {
            '_id': {
                'year': {
                    '$year': '$start_date'
                }, 
                'month': {
                    '$month': '$start_date'
                }
            }, 
            'activities': {
                '$sum': 1
            }
        }
    }, {
        '$sort': {
            'activities': -1
        }
    }, {
        '$limit': 1
    }
])

topUsersForMonth = connection.aggregate([
    {
        '$match': {
            '$and': [
                {
                    '$expr': {
                        '$eq': [
                            {
                                '$month': '$start_date'
                            }, 11
                        ]
                    }
                }, {
                    '$expr': {
                        '$eq': [
                            {
                                '$year': '$start_date'
                            }, 2008
                        ]
                    }
                }
            ]
        }
    }, {
        '$group': {
            '_id': '$user.id', 
            'activities': {
                '$sum': 1
            }, 
            'hours': {
                '$sum': {
                    '$divide': [
                        {
                            '$subtract': [
                                '$end_date', '$start_date'
                            ]
                        }, 3600000
                    ]
                }
            }
        }
    }, {
        '$sort': {
            'activities': -1
        }
    }, {
        '$limit': 5
    }
])

pprint(list(topYearMonth))
print()
pprint(list(topUsersForMonth))
