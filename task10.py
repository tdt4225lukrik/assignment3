"""
Find the total distance (in km) WALKED in 2008, by user with id=112.
"""
from DbConnector import DbConnector
from haversine import haversine, Unit


collection = DbConnector().db["Activity"]

result = collection.aggregate([
    {
        '$match': {
            'user.id': '112', 
            'transportation_mode': 'walk', 
            '$expr': {
                '$eq': [
                    {
                        '$year': '$start_date'
                    }, 2008
                ]
            }
        }
    }
])

distance = 0
for activity in result:
    for i in range(1, len(activity["trackPoints"])):
        lastPoint = activity["trackPoints"][i - 1]
        thisPoint = activity["trackPoints"][i]
        distance += haversine((lastPoint["lat"], lastPoint["lon"]), (thisPoint["lat"], thisPoint["lon"]), unit=Unit.METERS)
print(distance / 1000)

# print(list(result))