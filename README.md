# TDT4225 Assignment 3

## Model

We have chosen to use 1 collection, that is the Activity-collection. This collection is denormalized so that it has the user info for the activity and an array of each trackpoint that is under the activity. There are some advantages and disadvantages of doing it this way, but we have made the decision based on what we have to do in this assignment only.

### Advantages
The main advantages of having only a single collection that is denormalized, is that we do not have to do any application-level joins, which can be highly expensive, when doing the queries. It also makes the queries simpler because we only need to get the activity we want, and we have all the data related to that activity there. If we do not need to get all trackpoints though, we can simply just not select it in the queries. Since the exercise only asks us to do queries and not any inserts or updates, the redundancy of having it denormalized is not a problem either. Either way, the trackpoints for an activity is only related to one activity, so there is no redundancy there. One possible disadnvatage of having a large amount of items in an array is that we cannnot index the array well, so getting individual trackpoints will be more expensive. However, this is not really a problem because for most queries we want none of the trackpoints or all of the trackpoints for a particular activity. In addition, the array will never become larger than 2500 elements, meaning it will never exceed the document size limit of 16MB.

### Disadvantages
There are some users that do not have any activities or have only activities with over 2500 trackpoints. These users will not be registered in the database because they are not under any activity. This is the reason we get 173 users and not 181 users in the first query.