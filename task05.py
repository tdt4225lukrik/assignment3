"""
Find activities that are registered multiple times. You should find the query
even if you get zero results.
"""

from DbConnector import DbConnector

collection = DbConnector().db['Activity']

result = collection.aggregate([
    {
        '$group': {
            '_id': {
                'user': '$user.id', 
                'transportation_mode': '$transportation_mode', 
                'start_date': '$start_date', 
                'end_date': '$end_date', 
            }, 
            'count': {
                '$sum': 1
            }
        }
    }, {
        '$match': {
            '$expr': {
                '$gt': [
                    '$count', 1
                ]
            }
        }
    }
])

# Prints the number of duplicates, yet the result contains the duplicates themselves.
print("Duplicates: ", len(list(result)))