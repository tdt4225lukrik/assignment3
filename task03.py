"""
Find the top 10 users with the highest number of activities.
"""
from pprint import pprint
from DbConnector import DbConnector

collection = DbConnector().db['Activity']

result = collection.aggregate([
    {
        '$group': {
            '_id': '$user.id', 
            'activities': {
                '$sum': 1
            }
        }
    }, {
        '$sort': {
            'activities': -1
        }
    }, {
        '$limit': 10
    }
])

for doc in result:
    pprint(doc)