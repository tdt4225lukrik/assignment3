"""
Find the number of users that have started the activity in one day and ended
the activity the next day.
"""
import pprint
from DbConnector import DbConnector
from pprint import pprint

connection = DbConnector()
client = connection.client
db = connection.db
collection = db["Activity"]

result = collection.aggregate([
    {
        '$project': {
            'user_id': '$user.id', 
            'start_date_day': {
                '$dayOfYear': '$start_date'
            }, 
            'start_date_day_next': {
                '$add': [
                    {
                        '$dayOfYear': '$start_date'
                    }, 1
                ]
            }, 
            'end_date_day': {
                '$dayOfYear': '$end_date'
            }
        }
    }, {
        '$match': {
            '$expr': {
                '$eq': [
                    '$end_date_day', '$start_date_day_next'
                ]
            }
        }
    }, {
        '$group': {
            '_id': '$user_id'
        }
    }, {
        '$count': 'user_id'
    }
])

pprint(list(result))


