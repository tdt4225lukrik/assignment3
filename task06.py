"""
An infected person has been at position (lat, lon) (39.97548, 116.33031) at
time ‘2008-08-24 15:38:00’. Find the user_id(s) which have been close to this
person in time and space (pandemic tracking). Close is defined as the same
minute (60 seconds) and space (100 meters). (This is a simplification of the
“unsolvable” problem given i exercise 2).
"""
from datetime import date, datetime
from DbConnector import DbConnector
from tabulate import tabulate
from haversine import haversine, Unit

collection = DbConnector().db["Activity"]

# ---------------------------------------------------------------------------------------------
# Helper methods

def time_in_interval(time: datetime, start_time: datetime, end_time: datetime):
    return start_time <= time <= end_time

# Find if two trackpoints have overlap in space based on treshold (will be 100 meters here)
def pos_close(pos1, pos2, treshold):
    distance = haversine((pos1[0], pos1[1]), (pos2[0], pos2[1]), unit=Unit.METERS)
    return distance <= treshold

def pos_close_to_trackpoints(pos, trackpoints):
    for tp in trackpoints:
        if pos_close(pos, (tp["lat"], tp["lon"]), 100):
            return True

# ---------------------------------------------------------------------------------------------
# Here the main part begins

infe_time = datetime(2008, 8, 24, 15, 38, 0) # time of infected person
infe_pos = (39.97548, 116.33031)             # position of infected person

# Find all the activities that overlap in time
# so that we only have to check time and space overlap for the trackpoints in these activities
user_ids = []
for i in range(182):
    user_id = (str(i).rjust(3, "0"))
    # Find all the activities for the specific user (don't want to take all activities as it will take alot of memory)
    user_activities = collection.aggregate([
        {
            '$match': {
                'user.id': user_id
            }
        }
    ])

    # print("query done")

    # Go through all activities for this user and check if there is any that is close in both time and space
    for activity in user_activities:
        if time_in_interval(infe_time, activity["start_date"], activity["end_date"]):
            if pos_close_to_trackpoints(infe_pos, activity["trackPoints"]):
                user_ids.append(user_id)
                print("found match", user_id)
                break
    
    if user_id in user_ids:
        print(user_id, "has been close to the infected person")
    else:
        print(user_id, "not close")


print(user_ids)