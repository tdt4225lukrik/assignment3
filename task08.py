"""
Find all types of transportation modes and count how many distinct users that
have used the different transportation modes. Do not count the rows where the
transportation mode is null.
"""
from DbConnector import DbConnector
from pprint import pprint

collection = DbConnector().db["Activity"]

result = collection.aggregate([
    {
        '$match': {
            '$expr': {
                '$ne': [
                    '$transportation_mode', None
                ]
            }
        }
    }, {
        '$group': {
            '_id': {
                'transportation_mode': '$transportation_mode', 
                'user': '$user.id'
            }
        }
    }, {
        '$group': {
            '_id': '$_id.transportation_mode', 
            'number': {
                '$sum': 1
            }
        }
    }
])

pprint(list(result))